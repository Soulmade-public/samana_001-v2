﻿using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using System.Collections;

public class VideoManager : MonoBehaviour
{
    [Header("Video Clips")]
    [SerializeField] private VideoClip m_VideoClipRolstoelgebruik4k;
    [SerializeField] private VideoClip m_VideoClipSlechtziendheid4k;
    [SerializeField] private VideoClip m_VideoClipDementie4k;
    [SerializeField] private VideoClip m_VideoClipRolstoelgebruikHD;
    [SerializeField] private VideoClip m_VideoClipSlechtziendheidHD;
    [SerializeField] private VideoClip m_VideoClipDementieHD;

    [Header("Video Materials")]
    [SerializeField] private Material m_MaterialRolstoelgebruik4k;
    [SerializeField] private Material m_MaterialSlechtziendheid4k;
    [SerializeField] private Material m_MaterialDementie4k;
    [SerializeField] private Material m_MaterialRolstoelgebruikHD;
    [SerializeField] private Material m_MaterialSlechtziendheidHD;
    [SerializeField] private Material m_MaterialDementieHD;

    [Header("Video Render Textures")]
    [SerializeField] private RenderTexture m_RenderTextureRolstoelgebruik4k;
    [SerializeField] private RenderTexture m_RenderTextureSlechtziendheid4k;
    [SerializeField] private RenderTexture m_RenderTextureDementie4k;
    [SerializeField] private RenderTexture m_RenderTextureRolstoelgebruikHD;
    [SerializeField] private RenderTexture m_RenderTextureSlechtziendheidHD;
    [SerializeField] private RenderTexture m_RenderTextureDementieHD;


    [Header("Other")]
    [SerializeField] private GameObject m_VideoSphereObj;
    private VideoPlayer m_VideoPlayer;
    private int m_SelectedVideoClipID;
    public GameObject cv;
    void Start()
    {
        m_VideoPlayer = m_VideoSphereObj.GetComponent<VideoPlayer>();

        m_SelectedVideoClipID = PlayerPrefs.GetInt("VideoNumber");
        Load4KVideo(m_SelectedVideoClipID);

        StartCoroutine("CheckIfVideoIsPlaying");
        m_VideoPlayer.loopPointReached += WhenVideoIsDone;
    }

    private void WhenVideoIsDone(VideoPlayer vp)
    {
        SceneManager.LoadScene(0);
    }

    void Load4KVideo(int selectedVideoClipID)
    {
        m_VideoPlayer.Prepare();
        switch (selectedVideoClipID)
        {
            case 1:
                m_VideoPlayer.clip = m_VideoClipRolstoelgebruik4k;
                m_VideoSphereObj.GetComponent<Renderer>().material = m_MaterialRolstoelgebruik4k;
                m_VideoPlayer.targetTexture = m_RenderTextureRolstoelgebruik4k;             
                break;

            case 2:
                m_VideoPlayer.clip = m_VideoClipSlechtziendheid4k;
                m_VideoSphereObj.GetComponent<Renderer>().material = m_MaterialSlechtziendheid4k;
                m_VideoPlayer.targetTexture = m_RenderTextureSlechtziendheid4k;
                break;

            case 3:
                m_VideoPlayer.clip = m_VideoClipDementie4k;
                m_VideoSphereObj.GetComponent<Renderer>().material = m_MaterialDementie4k;
                m_VideoPlayer.targetTexture = m_RenderTextureDementie4k;
                break;

            default:
                print("ERROR: NO 4K VIDEO SELECTED!");
                break;
        }
    }

    void LoadHDVideo(int selectedVideoClipID)
    {
        m_VideoPlayer.Prepare();
        switch (selectedVideoClipID)
        {
            case 1:
                m_VideoPlayer.clip = m_VideoClipRolstoelgebruikHD;
                m_VideoSphereObj.GetComponent<Renderer>().material = m_MaterialRolstoelgebruikHD;
                m_VideoPlayer.targetTexture = m_RenderTextureRolstoelgebruikHD;
                break;

            case 2:
                m_VideoPlayer.clip = m_VideoClipSlechtziendheidHD;
                m_VideoSphereObj.GetComponent<Renderer>().material = m_MaterialSlechtziendheidHD;
                m_VideoPlayer.targetTexture = m_RenderTextureSlechtziendheidHD;
                break;

            case 3:
                m_VideoPlayer.clip = m_VideoClipDementieHD;
                m_VideoSphereObj.GetComponent<Renderer>().material = m_MaterialDementieHD;
                m_VideoPlayer.targetTexture = m_RenderTextureDementieHD;
                break;

            default:
                print("ERROR: NO HD VIDEO SELECTED!");
                break;
        }
    }

    IEnumerator CheckIfVideoIsPlaying()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.5f);
            if (!m_VideoPlayer.isPlaying)
            {
                print("LOADED HD VERSION");
                LoadHDVideo(m_SelectedVideoClipID);
                cv.SetActive(true);
            }
            else
            {
                print("PLAYING 4K VERSION");
            }

        }
    }
}


