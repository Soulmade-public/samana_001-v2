﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

[System.Serializable]
public class MenuManager : MonoBehaviour
{
    [SerializeField] private GameObject m_InstructieCanvas;
    [SerializeField] private GameObject m_VrSceneObj;
    [SerializeField] private Camera m_MainCamera;

    private MagicModeCamera m_MagicModeScript;

    private void Start()
    {
        m_MagicModeScript = m_MainCamera.GetComponent<MagicModeCamera>();
        StartCoroutine(LoadDevice("None"));
        PlayerPrefs.SetInt("isCardboardActive", 0);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void EnterMagicMode()
    {
        m_MagicModeScript.enabled = true;
        m_VrSceneObj.SetActive(true);
        m_InstructieCanvas.SetActive(false);
    }

    public void ExitMagicMode()
    {
        SceneManager.LoadScene(0);  
    }

    public void ToggleVR()
    {      
        StartCoroutine(LoadDevice("cardboard"));
        PlayerPrefs.SetInt("isCardboardActive", 1);
        m_InstructieCanvas.SetActive(false);
        m_VrSceneObj.SetActive(true);
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    IEnumerator LoadDevice(string newDevice)
    {
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        XRSettings.enabled = true;
    }
}
