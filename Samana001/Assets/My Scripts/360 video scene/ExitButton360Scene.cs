﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitButton360Scene : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);         
        }
    }
}
