﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class MenuManagerOculusGO : MonoBehaviour
{
    [SerializeField] private Camera m_PlayerCamera;
    [SerializeField] private GameObject m_GazeRing;
    [SerializeField] private GameObject m_Reticle;
    [SerializeField] private GameObject m_AudioManagerObj;
    [SerializeField] private GameObject m_ButtonCanvas;
    [SerializeField] private GameObject m_ReticleCanvas;
    [SerializeField] private GameObject m_MagicCanvas;
    [SerializeField] private GameObject m_VideoSphere;

    private VideoManager m_VideoManagerScript;
    private MagicModeCamera m_MagicModeScript;
    private AudioSource m_AudioSource;
    private Image m_Img;
    private bool m_IsSelectingButton = false;
    private bool m_isButtonSoundPLayed = false;

    private void Start()
    {
        m_Img = m_GazeRing.GetComponent<Image>();
        m_VideoManagerScript = m_VideoSphere.GetComponent<VideoManager>();
        m_MagicModeScript = m_PlayerCamera.GetComponent<MagicModeCamera>();

        if (PlayerPrefs.GetInt("isCardboardActive") == 0)
        {
            m_MagicModeScript.enabled = true;
            m_MagicCanvas.SetActive(true);
        }
    }

    void Update()
    {
        //Fill gaze reticle when selecting button press.
        //if (m_IsSelectingButton)
        //{
        //    m_Img.fillAmount += 0.7f * Time.deltaTime;
        //}

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        //else if (Input.GetKeyDown(KeyCode.Escape) && m_VideoManagerScript.enabled)
        //{
        //    ExitVideo();
        //}

       // GazingRaycast();
    }

    // Gazing with raycasts.
    void GazingRaycast()
    {
        RaycastHit hit;

        if (Physics.Raycast(m_PlayerCamera.transform.position, m_PlayerCamera.transform.forward, out hit) && hit.collider.gameObject.tag == "GazeObject")
        {
            StartCoroutine(WaitOneSecond(hit));
            m_IsSelectingButton = true;
            m_Reticle.SetActive(false);
        }
        else
        {
            StopAllCoroutines();
            m_IsSelectingButton = false;
            m_Reticle.SetActive(true);
            m_Img.fillAmount = 0f;
        }
    }

    // Wait one second for a button confirm.
    IEnumerator WaitOneSecond(RaycastHit hit)
    {
        yield return new WaitForSeconds(1.5f);

        // Check what video is selected.
        //if (hit.collider.gameObject.name == "MainMenu" && m_IsSelectingButton == true)
        //{
        //    ReturnToMainMenu();
        //}
        //else if (hit.collider.gameObject.name == "PlayVideo" && m_IsSelectingButton == true)
        //{
        //    PlayVideo();
        //}
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void PlayVideo()
    {
        //m_ButtonCanvas.SetActive(false);
        //m_ReticleCanvas.SetActive(false);
        //m_VideoManagerScript.enabled = true;
    }

    public void ExitMagicCanvasButton()
    {
        SceneManager.LoadScene(0);
    }

    private void ExitVideo()
    {
        SceneManager.LoadScene(1);
    }
    IEnumerator LoadDevice(string newDevice)
    {
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        XRSettings.enabled = true;
    }
}
